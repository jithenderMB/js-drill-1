const userObject = require("../cars.js");
const problem1 = require("../problem1.js");

const userArray = userObject.inventory;

const result = problem1(userArray,33);

if (result.length !== 0 && typeof result === 'object'){
    console.log(`Car 33 is a ${result.car_year} ${result.car_make} ${result.car_model}`);
}
else{
    console.log(result);
}

