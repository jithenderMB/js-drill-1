function findTheLastCarDetails(data){
    if (typeof data === 'object'){//last element index value is length-1
    indexOfCar = data.length - 1;
    const lastCar = data[indexOfCar];
    return `Last car is a ${lastCar.car_make} ${lastCar.car_model}`}
    else{
        return 'Enter a valid input';
    }
}

module.exports = findTheLastCarDetails;



// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of: 
//"Last car is a *car make goes here* *car model goes here*"