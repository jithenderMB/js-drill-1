function sortModelAlphabetically(data){
    if (typeof data === 'object'){

        /*we duplicated data array to prevent overriding array
        we used [...array] to duplicate*/
        const duplicateDataArray = [...data];

        /*function in sort built-in function sorts data by returning number as -1,0,1
        -1 sorts to front 
        1 sorts to back 
        0 dont change position
        */
        const alphabeticalCarModels = duplicateDataArray.sort((a,b) => {
            if (a.car_model.toLowerCase() < b.car_model.toLowerCase()) return -1;
            if (a.car_model.toLowerCase() > b.car_model.toLowerCase()) return 1;
            return 0;
        });

        return alphabeticalCarModels;
    }
    else{
        return 'Enter a valid input';
    }

}

module.exports = sortModelAlphabetically;

// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.
