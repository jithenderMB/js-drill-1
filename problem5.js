function getCarDetails(data,year){
    //this function takes cars array and year and returns car deatils of particular year
    
    for (let i=0;i<data.length;i++){
        let user = data[i];
        if (user.car_year === year){
            return user;
        }
    }
}


function carsDetailsBefore2000(data,years){ 
    //using years array the data is filtered
    if (typeof data === 'object' && typeof years === 'object'){ 
        carsBefore2000 = [];
        for (let i=0;i<years.length;i++){
            let year = years[i];
            console.log(typeof year);
            if (typeof year === 'number' && year < 2000){
                carsBefore2000.push(getCarDetails(data,year));
            }
        }
        return carsBefore2000;
    }
    else{
        return 'Enter a valid input';
    }
}

module.exports = carsDetailsBefore2000;



// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.
