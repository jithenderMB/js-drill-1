function getBmwAndAudiCarDetails(data){
    if (typeof data === 'object'){ 
        BMWAndAudi = [];
        for (let i=0;i<data.length;i++){
            let user = data[i];
            if (user.car_make === 'BMW' || user.car_make === 'Audi'){
                BMWAndAudi.push(user);
            }
        }
        return JSON.stringify(BMWAndAudi);
    }
    else{
        return 'Enter a valid input';
    }
}

module.exports = getBmwAndAudiCarDetails;



// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.
